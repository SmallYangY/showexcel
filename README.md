# excelshow

> A Vue.js project

## 描述

通过获取excel文件网址，对excel进行表格展示

未测试过单元格合并情况

通过query传参:

- path excel网络地址
- normal true表示一般表格，false为项目特殊双表格，可自行进行扩展

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
